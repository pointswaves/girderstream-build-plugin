#Download base image ubuntu 20.04
FROM debian:stable

# Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

# Update Ubuntu Software repository
RUN apt update; apt install -y build-essential curl && \
    rm -rf /var/lib/apt/lists/* && \
    apt clean

RUN curl https://sh.rustup.rs -sSf | sh -s -- -y
ENV PATH="/root/.cargo/bin:${PATH}"
#RUN rustup target add armv7-unknown-linux-gnueabihf


RUN mkdir /build/
WORKDIR /build/


#COPY entrypoint.sh /usr/local/bin/entrypoint.sh
#RUN chmod +x /usr/local/bin/entrypoint.sh

#ENV CARGO_HOME=/root/.cargo/
#ENV RUSTUP_HOME=/root/.rustup/

#ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
#
#RUN pwd
#RUN ls -la
#RUN cargo build --release --target=armv7-unknown-linux-gnueabihf



