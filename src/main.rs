use std::collections::HashMap;

use clap::{self, Parser};
use linked_hash_map::LinkedHashMap;
use serde::{Deserialize, Serialize};
use tokio::{
    fs,
    io::{AsyncReadExt, AsyncWriteExt},
};

/// Simple program to create git commands
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Name of the person to greet
    #[clap(short, long)]
    plugin_type: String,
    /// Name of the person to greet
    #[clap(short, long)]
    variables_file: String,
}

#[derive(Debug, PartialEq, Deserialize, Serialize)]
struct PluginFile {
    build_steps: LinkedHashMap<String, Vec<String>>,
    build_variables: HashMap<String, String>,
}

#[derive(Debug, PartialEq, Deserialize, Serialize)]
struct PluginVariables {
    plugin_vars: HashMap<String, String>,
}

fn get_resolved_var(
    var: String,
    plugin_map: &HashMap<String, String>,
    build_map: &HashMap<String, String>,
) -> String {
    if let Some(found) = var.find("%{") {
        let end = var[found..]
            .find('}')
            .expect("variable end marker not found")
            + found;
        let sub = &var[(found + 2)..end];
        let new = if let Some(nv) = build_map.get(sub) {
            nv
        } else if let Some(nv) = plugin_map.get(sub) {
            nv
        } else {
            panic!("could not find var sub: {sub}")
        };
        let new = format!("{}{}{}", &var[..found], new, &var[(end + 1)..]);
        get_resolved_var(new, plugin_map, build_map)
    } else {
        var
    }
}

pub type Crapshoot = anyhow::Result<()>;

#[tokio::main]
async fn main() -> Crapshoot {
    let args = Args::parse();

    let mut fl = fs::File::open(args.plugin_type).await?;
    let mut buff = vec![];
    fl.read_to_end(&mut buff).await?;
    let string_plugin = std::str::from_utf8(&buff).unwrap();
    let plugin_builtin: PluginFile =
        serde_yaml::from_str(string_plugin).expect("Could parse yaml plugin file");

    let mut fl = fs::File::open(args.variables_file).await?;
    let mut buff = vec![];
    fl.read_to_end(&mut buff).await?;
    let string_plugin_vars = std::str::from_utf8(&buff).unwrap();
    let plugin_vars: PluginVariables =
        serde_yaml::from_str(string_plugin_vars).expect("Could parse yaml plugin vars");

    let mut all_commands = "".to_string();
    let plugin_var_vars = plugin_vars.plugin_vars.clone();

    for (name, step) in &plugin_builtin.build_steps {
        println!("{name} {step:?}");

        let config_line_name = format!("{}{}", "config_", name);
        let config_line_name_amend = format!("{}{}", "configA_", name);
        let config_line_name_prepend = format!("{}{}", "configP_", name);

        let step: Vec<String> = if let Some(config) = plugin_var_vars.get(&config_line_name) {
            // we could maybe have some logic for if we replace or append
            vec![config.clone()]
        } else if let Some(config) = plugin_var_vars.get(&config_line_name_amend) {
            let mut new = vec![config.clone()];
            new.extend(step.clone());
            new
        } else if let Some(config) = plugin_var_vars.get(&config_line_name_prepend) {
            let mut new = step.clone();
            new.extend(vec![config.clone()]);
            new
        } else {
            step.clone()
        };

        for line in step {
            all_commands += &format!(
                "{}\n",
                get_resolved_var(line, &plugin_builtin.build_variables, &plugin_var_vars)
            );
        }

        //let re = Regex::new(r"(?P<last>[^,\s]+),\s+(\S+)").unwrap();
    }

    let mut fl = fs::File::create("configured.sh").await?;

    fl.write_all(b"set -eux pipefail\n").await.unwrap();
    for env_var in std::env::vars() {
        fl.write_all(format!("export \"{}\"=\"{}\"\n", env_var.0, get_resolved_var(env_var.1, &plugin_builtin.build_variables, &plugin_var_vars)).as_bytes()).await.unwrap();
    }

    fl.write_all(all_commands.as_bytes()).await.unwrap();

    Ok(())
}
